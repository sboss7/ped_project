import { Button } from '@components';
import { useState } from 'react';
import './style.scss';

export default function HomePage() {
    const [count, setCount] = useState(404);

    return (
        <div className="home">
            <h1>Hello World...</h1>

            <h1>{count}</h1>
            <div className="home__button-groups">
                <Button onClick={() => setCount((current_value) => current_value + 1)}>
                    +1
                </Button>

                <Button
                    variant="secondary"
                    onClick={() => setCount((current_value) => current_value - 1)}
                >
                    -1
                </Button>
            </div>
        </div>
    );
}
