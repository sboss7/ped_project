import { HomePage, NotFoundPage } from '@pages';

import { Routes, Route } from 'react-router-dom';

export default function Router() {
    return (
        <Routes>
            <Route path="/">
                <Route index element={<HomePage />} />
                <Route path="*" element={<NotFoundPage />} />
            </Route>
        </Routes>
    );
}
