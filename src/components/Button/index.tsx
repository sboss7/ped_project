import './style.scss';

interface IProps {
    children: JSX.Element | string;
    onClick?: React.MouseEventHandler<HTMLButtonElement>;
    variant?: 'primary' | 'secondary';
}

export default function Button({ children, onClick, variant = 'primary' }: IProps) {
    return (
        <button className={`button button--${variant}`} onClick={onClick}>
            {children}
        </button>
    );
    // return
}
