import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import Routers from './routes';
import { BrowserRouter } from 'react-router-dom';

const root_element = document.getElementById('root') as HTMLElement;

const root = ReactDOM.createRoot(root_element);

root.render(
    <React.StrictMode>
        <BrowserRouter>
            <Routers />
        </BrowserRouter>
    </React.StrictMode>,
);
