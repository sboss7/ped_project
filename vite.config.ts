import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
    server: {
        host: '0.0.0.0',
        port: 8080,
    },
    plugins: [react()],
    resolve: {
        alias: {
            '@pages': path.resolve(__dirname, 'src/pages'),
            '@components': path.resolve(__dirname, 'src/components'),
            '@hooks'     :  path.resolve(__dirname, 'src/hooks'),
            '@data'      : path.resolve(__dirname, 'src/data'),
            '@assets'    : path.resolve(__dirname, 'src/assets'),
            '@store'     : path.resolve(__dirname, 'src/store'),
            '@utils'     : path.resolve(__dirname, 'src/utils'),
        },
    },
});
