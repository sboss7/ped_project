FROM node:lts

WORKDIR /app

COPY . .
RUN npm install -g npm@9.2.0
CMD npm install && npm run dev